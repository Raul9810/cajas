#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <SPI.h>
#include <MFRC522.h>

bool cargando = false;

/*
  RFID 1:"2C:3A:E8:35:D3:D1";
  RFID 2:"2C:3A:E8:33:ED:10";
  RFID 3:"2C:3A:E8:35:DD:6F";
*/

// Update these with values suitable for your network.
const int BLUE = D0;
const int RED = D3;
const int GREEN = D4;
const int BUZ = D8;

char* ssid = "redUAQ";
char* password = "";

char* mqtt_server = "148.220.52.100";

char topicoEnvio[22];
char topicoEnvioCB[30];
char topicoRegistro[30];//26/

String MACstring;
String stringTopicoEnvio;
String stringTopicoEnvioCB;
String stringTopicoRegistro;

#define RST_PIN D1    //Pin 9 para el reset del RC522
#define SS_PIN D2   //Pin 10 para el SS (SDA) del RC522
MFRC522 mfrc522(SS_PIN, RST_PIN); //Creamos el objeto para el RC522



WiFiClient espClient;
PubSubClient client(espClient);
String pares;
String iniciar;
//char par[8];

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      // client.subscribe("RFID/CB");
      client.subscribe(topicoEnvioCB); //callback 1,2,3
      client.setCallback(callback); //callback MAC
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(topicoRegistro, "Listo");
      // ... and resubscribe

    } else {
      digitalWrite(RED, 1);
      delay(500);
      digitalWrite(RED, 0);

      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(2000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int lengt) {
  cargando = false;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < lengt; i++) {
    //Serial.print((String)payload[i]);
    Serial.println(topic);
  }
  Serial.println(payload[0]);

  switch (payload[0]) {



    case '1':


      analogWrite(GREEN, 255);
      digitalWrite(BUZ, HIGH);
      delay (200);
      analogWrite(BUZ, LOW);
      delay(500);
      analogWrite(GREEN, 0);
      break;
    case '2':

      for (int i = 0; i < 3; i++) {
        analogWrite(RED, 255);
        digitalWrite(BUZ, HIGH);
        delay (50);
        digitalWrite(BUZ, LOW);
        analogWrite(RED, LOW);
        delay (50);
      }

      break;
    case '3':

      iniciar = "3";


      analogWrite(GREEN, 255);
      digitalWrite(BUZ, HIGH);
      delay (500);
      digitalWrite(BUZ, LOW);
      delay(500);
      analogWrite(GREEN, 0);

      break;



    case '0':

      for (int i = 0; i < 2; i++) {
        analogWrite(RED, 255);
        analogWrite(GREEN, 249);
        analogWrite(BLUE, 51);
        digitalWrite(BUZ, HIGH);
        delay (100);
        digitalWrite(BUZ, LOW);
        analogWrite(RED, 0);
        analogWrite(GREEN, 0);
        analogWrite(BLUE, 0);
        delay (100);
      }

      break;

      // statements




  }
}

void setup() {
  Serial.begin(115200);
  SPI.begin();        //Iniciamos el Bus SPI
  mfrc522.PCD_Init(); // Iniciamos  el MFRC522
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  //Serial.println(pares);
  Serial.print("MAC: ");
  Serial.println(WiFi.macAddress());

  String MACstring = WiFi.macAddress();
  stringTopicoEnvio = "RFID/" + MACstring;
  stringTopicoEnvioCB = "RFIDCB/" + MACstring;
  stringTopicoRegistro = "registro/" + MACstring;

  for (int i = 0; i < stringTopicoRegistro.length(); i++) {
    topicoRegistro[i] = stringTopicoRegistro[i];
  }
  for (int i = 0; i < stringTopicoEnvioCB.length(); i++) {
    topicoEnvioCB[i] = stringTopicoEnvioCB[i];
  }
  for (int i = 0; i < stringTopicoEnvio.length(); i++) {
    topicoEnvio[i] = stringTopicoEnvio[i];
  }
  Serial.println((String)topicoEnvio);
  Serial.println((String)topicoEnvioCB);
  Serial.println((String)topicoRegistro);

}


void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (iniciar == "3") {
    if (cargando) {
      for (int i = 0; i < 1; i++) {
        analogWrite(BLUE, 255);
        analogWrite(BUZ, 110);
        delay (200);
        analogWrite(BUZ, 0);
        analogWrite(BLUE, 0);
        delay(200);
      }
    }

    if (mfrc522.PICC_IsNewCardPresent())
    {

      //Seleccionamos una tarjeta


      if ( mfrc522.PICC_ReadCardSerial())
      {

        // Enviamos por serial su UID
        Serial.print("Card UID:");
        pares = "";
        for (byte i = 0; i < mfrc522.uid.size; i++) {

          Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
          Serial.print(mfrc522.uid.uidByte[i], HEX);

          pares += mfrc522.uid.uidByte[i] < 0x10 ? "0" : "";
          pares += String(mfrc522.uid.uidByte[i], HEX);

        }
        pares += "\n";



        // Terminamos la lectura de la tarjeta  actual
        Serial.println(" ");
        // Serial.println(pares);
        client.publish(topicoEnvio, pares.c_str());
        cargando = true;

        /*
          digitalWrite(GREEN,1);
          delay(500);
          digitalWrite(GREEN,0);
        */
        mfrc522.PICC_HaltA();

      }
    }
  }
  else {
    for (int i = 0; i < 1; i++) {
      analogWrite(BLUE, 255);
      analogWrite(BUZ, 110);
      delay (200);
      digitalWrite(BUZ, LOW);
      analogWrite(BLUE, 0);
      delay(200);





    }
  }
}
